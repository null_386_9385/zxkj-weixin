package com.zxkj.ssm.weixin.shop.basic;
import java.util.List;

import com.zxkj.ssm.weixin.shop.model.PagerModel;
import org.springframework.transaction.annotation.Transactional;
import  static  com.zxkj.ssm.weixin.shop.basic.Base.notEmpty;
@Transactional(readOnly = true)
public abstract class BasicServerImpl<E, DAO extends  BasicDao<E>>  implements BasicService<E>{
	protected DAO dao;
	public DAO getDao() {
		return dao;
	}

	public abstract void setDao(DAO dao);

	/**
	 * 添加
	 * 
	 * @param e
	 * @return
	 */
	@Transactional(readOnly = false)
	public int insert(E e) {
		if(e==null){
			throw new NullPointerException();
		}
		return dao.insert(e);
	}

	@Override
	public int insertSelective(E e) {
		if(e==null){
			throw new NullPointerException();
		}
		return dao.insertSelective(e);
	}

	/**
	 * 删除
	 * 
	 * @param e
	 * @return
	 */
	@Transactional(readOnly = false)
	public int delete(E e) {
		if(e==null){
			throw new NullPointerException();
		}
		return dao.delete(e);
	}

	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @return
	 */
	@Transactional(readOnly = false)
	public int deletes(Integer[] ids) {
		if (ids == null || ids.length == 0) {
			throw new NullPointerException("id不能全为空！");
		}
		
		for (int i = 0; i < ids.length; i++) {
			if(Base.empty(ids[i])){
				throw new NullPointerException("id不能为空！");
			}
			dao.deleteById(ids[i]);
		}
		return 0;
	}

	public int deleteBatch(List<E> list){
		if(notEmpty(list)){
		return dao.deleteBatch(list);
		}else{
			throw new NullPointerException("id不能全为空！");
		}
	}

	/**
	 * 修改
	 * 
	 * @param e
	 * @return
	 */
	@Transactional(readOnly = false)
	public int update(E e) {
		if(e==null){
			throw new NullPointerException();
		}
		return dao.update(e);
	}
	
	
	@Override
	public int updateByPrimaryKeySelective(E e) {
		if(e==null){
			throw new NullPointerException();
		}
		return dao.updateByPrimaryKeySelective(e);
	}

	/**
	 * 查询一条记录
	 * 
	 * @param e
	 * @return
	 */
	public E selectOne(E e) {
		return dao.selectOne(e);
	}

	/**
	 * 分页查询
	 * 
	 * @param e
	 * @return
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public PagerModel selectPageList(E e) {
		List list = dao.selectPageList(e);
		PagerModel pm = new PagerModel();
		pm.setList(list);
		Object oneC = dao.selectPageCount(e);
		if(oneC!=null){
			pm.setTotal(Integer.parseInt(oneC.toString()));
		}else{
			pm.setTotal(0);
		}
		return pm;
	}
	
	public List<E> selectList(E e) {
		return dao.selectList(e);
	}

	@Override
	public E selectById(Integer id) {
		return dao.selectById(id);
	}
}
