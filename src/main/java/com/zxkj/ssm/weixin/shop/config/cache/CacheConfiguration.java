package com.zxkj.ssm.weixin.shop.config.cache;
import com.zxkj.ssm.weixin.shop.cache.impl.EhcacheCacheProvider;
import com.zxkj.ssm.weixin.shop.cache.impl.SystemManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

@Configuration
public class CacheConfiguration {
    @Autowired
    private Environment env;
    @Bean
    public EhcacheCacheProvider initEhcacheCacheProvider() {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        EhcacheCacheProvider provider = new EhcacheCacheProvider();
        provider.setCacheName(env.getProperty("cache.file.name"));
        provider.setConfigLocation(resolver.getResource(env.getProperty("cache.file.path")));
        return provider;
    }

    @Bean(name="systemManager")
    public SystemManager initSystemManager(){
        SystemManager systemManager=new SystemManager();
        systemManager.setCacheProvider(initEhcacheCacheProvider());
        return systemManager;
    }

}
