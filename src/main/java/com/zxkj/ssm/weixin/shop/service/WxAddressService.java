package com.zxkj.ssm.weixin.shop.service;
import  com.zxkj.ssm.weixin.shop.basic.BasicService;
import  com.zxkj.ssm.weixin.shop.entity.WxAddress;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxAddressService
*@Version
*/
public interface WxAddressService extends BasicService<WxAddress> {

    }
