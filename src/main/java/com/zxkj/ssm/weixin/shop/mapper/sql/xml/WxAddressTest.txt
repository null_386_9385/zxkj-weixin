目标package: com.zxkj.ssm.weixin.shop.mapper.sql.xml

当前时间：
2018-7-10
17:23:22
2018-07-10 17:23:22

所有配置的属性信息:
targetPackage - com.zxkj.ssm.weixin.shop.mapper.sql.xml
templateFormatter - com.zxkj.ssm.weixin.shop.mybatis.generator.formatter.FreemarkerTemplateFormatter
templatePath - template/test/test-one.ftl
targetProject - D:/EclipseWorkspace2/zxkj-weixin/src/main/java
fileName - ${tableClass.shortClassName}Test.txt

实体和表的信息：
表名：wx_address
变量名：wxAddress
小写名：wxaddress
类名：WxAddress
全名：com.zxkj.ssm.weixin.shop.entity.WxAddress
包名：com.zxkj.ssm.weixin.shop.entity

列的信息：
=====================================
主键：
    -------------------------------------
    列名：address_id
    列类型：INTEGER
    字段名：addressId
    注释：
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0

基础列：
    -------------------------------------
    列名：wu_id
    列类型：INTEGER
    字段名：wuId
    注释：微信用户ID
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0
    -------------------------------------
    列名：address_province
    列类型：VARCHAR
    字段名：addressProvince
    注释：省份
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：50
    列精度：0
    -------------------------------------
    列名：address_city
    列类型：VARCHAR
    字段名：addressCity
    注释：城市
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：50
    列精度：0
    -------------------------------------
    列名：address_country
    列类型：VARCHAR
    字段名：addressCountry
    注释：县（区）
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：50
    列精度：0
    -------------------------------------
    列名：address_road
    列类型：VARCHAR
    字段名：addressRoad
    注释：街道
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：100
    列精度：0
    -------------------------------------
    列名：address_full
    列类型：VARCHAR
    字段名：addressFull
    注释：完整地址
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：200
    列精度：0
    -------------------------------------
    列名：telephone
    列类型：VARCHAR
    字段名：telephone
    注释：电话号码
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：20
    列精度：0
    -------------------------------------
    列名：truename
    列类型：VARCHAR
    字段名：truename
    注释：姓名
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：50
    列精度：0
    -------------------------------------
    列名：post_code
    列类型：VARCHAR
    字段名：postCode
    注释：邮编
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：true
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0
    -------------------------------------
    列名：isDefault
    列类型：BIT
    字段名：isdefault
    注释：1：是0：否 首次添加的地址设置为默认地址
    类型包名：java.lang
    类型短名：Boolean
    类型全名：java.lang.Boolean
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：1
    列精度：0

Blob列：

=====================================
全部列：
列名 - 字段名
    address_id - addressId
    wu_id - wuId
    address_province - addressProvince
    address_city - addressCity
    address_country - addressCountry
    address_road - addressRoad
    address_full - addressFull
    telephone - telephone
    truename - truename
    post_code - postCode
    isDefault - isdefault
