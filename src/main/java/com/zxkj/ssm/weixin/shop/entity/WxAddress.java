package com.zxkj.ssm.weixin.shop.entity;

import com.zxkj.ssm.weixin.shop.model.PagerModel;
import java.io.Serializable;

public class WxAddress extends PagerModel implements Serializable {
    private Integer addressId;

    /**
     * 微信用户ID
     *
     * @mbg.generated
     */
    private Integer wuId;

    /**
     * 省份
     *
     * @mbg.generated
     */
    private String addressProvince;

    /**
     * 城市
     *
     * @mbg.generated
     */
    private String addressCity;

    /**
     * 县（区）
     *
     * @mbg.generated
     */
    private String addressCountry;

    /**
     * 街道
     *
     * @mbg.generated
     */
    private String addressRoad;

    /**
     * 完整地址
     *
     * @mbg.generated
     */
    private String addressFull;

    /**
     * 电话号码
     *
     * @mbg.generated
     */
    private String telephone;

    /**
     * 姓名
     *
     * @mbg.generated
     */
    private String truename;

    /**
     * 邮编
     *
     * @mbg.generated
     */
    private String postCode;

    /**
     * 1：是0：否 首次添加的地址设置为默认地址
     *
     * @mbg.generated
     */
    private Boolean isdefault;

    private static final long serialVersionUID = 1L;

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public Integer getWuId() {
        return wuId;
    }

    public void setWuId(Integer wuId) {
        this.wuId = wuId;
    }

    public String getAddressProvince() {
        return addressProvince;
    }

    public void setAddressProvince(String addressProvince) {
        this.addressProvince = addressProvince;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

    public void setAddressCountry(String addressCountry) {
        this.addressCountry = addressCountry;
    }

    public String getAddressRoad() {
        return addressRoad;
    }

    public void setAddressRoad(String addressRoad) {
        this.addressRoad = addressRoad;
    }

    public String getAddressFull() {
        return addressFull;
    }

    public void setAddressFull(String addressFull) {
        this.addressFull = addressFull;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public Boolean getIsdefault() {
        return isdefault;
    }

    public void setIsdefault(Boolean isdefault) {
        this.isdefault = isdefault;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", addressId=").append(addressId);
        sb.append(", wuId=").append(wuId);
        sb.append(", addressProvince=").append(addressProvince);
        sb.append(", addressCity=").append(addressCity);
        sb.append(", addressCountry=").append(addressCountry);
        sb.append(", addressRoad=").append(addressRoad);
        sb.append(", addressFull=").append(addressFull);
        sb.append(", telephone=").append(telephone);
        sb.append(", truename=").append(truename);
        sb.append(", postCode=").append(postCode);
        sb.append(", isdefault=").append(isdefault);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        WxAddress other = (WxAddress) that;
        return (this.getAddressId() == null ? other.getAddressId() == null : this.getAddressId().equals(other.getAddressId()))
            && (this.getWuId() == null ? other.getWuId() == null : this.getWuId().equals(other.getWuId()))
            && (this.getAddressProvince() == null ? other.getAddressProvince() == null : this.getAddressProvince().equals(other.getAddressProvince()))
            && (this.getAddressCity() == null ? other.getAddressCity() == null : this.getAddressCity().equals(other.getAddressCity()))
            && (this.getAddressCountry() == null ? other.getAddressCountry() == null : this.getAddressCountry().equals(other.getAddressCountry()))
            && (this.getAddressRoad() == null ? other.getAddressRoad() == null : this.getAddressRoad().equals(other.getAddressRoad()))
            && (this.getAddressFull() == null ? other.getAddressFull() == null : this.getAddressFull().equals(other.getAddressFull()))
            && (this.getTelephone() == null ? other.getTelephone() == null : this.getTelephone().equals(other.getTelephone()))
            && (this.getTruename() == null ? other.getTruename() == null : this.getTruename().equals(other.getTruename()))
            && (this.getPostCode() == null ? other.getPostCode() == null : this.getPostCode().equals(other.getPostCode()))
            && (this.getIsdefault() == null ? other.getIsdefault() == null : this.getIsdefault().equals(other.getIsdefault()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getAddressId() == null) ? 0 : getAddressId().hashCode());
        result = prime * result + ((getWuId() == null) ? 0 : getWuId().hashCode());
        result = prime * result + ((getAddressProvince() == null) ? 0 : getAddressProvince().hashCode());
        result = prime * result + ((getAddressCity() == null) ? 0 : getAddressCity().hashCode());
        result = prime * result + ((getAddressCountry() == null) ? 0 : getAddressCountry().hashCode());
        result = prime * result + ((getAddressRoad() == null) ? 0 : getAddressRoad().hashCode());
        result = prime * result + ((getAddressFull() == null) ? 0 : getAddressFull().hashCode());
        result = prime * result + ((getTelephone() == null) ? 0 : getTelephone().hashCode());
        result = prime * result + ((getTruename() == null) ? 0 : getTruename().hashCode());
        result = prime * result + ((getPostCode() == null) ? 0 : getPostCode().hashCode());
        result = prime * result + ((getIsdefault() == null) ? 0 : getIsdefault().hashCode());
        return result;
    }
}