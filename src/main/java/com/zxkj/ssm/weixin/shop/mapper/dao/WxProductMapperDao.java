package com.zxkj.ssm.weixin.shop.mapper.dao;
import com.zxkj.ssm.weixin.shop.basic.BasicDao;
import com.zxkj.ssm.weixin.shop.entity.WxProduct;

/**
* 通用 Mapper 代码生成器
*/
public interface WxProductMapperDao extends BasicDao<WxProduct> {

}




