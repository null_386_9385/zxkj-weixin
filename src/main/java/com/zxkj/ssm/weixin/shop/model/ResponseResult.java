package com.zxkj.ssm.weixin.shop.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 程序媛
 * @2018年6月24日
 * @Description: 响应码
 */
@Data
public class ResponseResult implements Serializable{

    private static final long serialVersionUID = 1L;

    private  Object data;

    private String message;

    private Integer statusCode;

    private Date time;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    public Date  getTime(){
        return time;
    }

    public ResponseResult  setStatusCode(Integer statusCode) {
        this.statusCode=statusCode;
        return this;
    }

    public ResponseResult  setMessage(String message) {
        this.message=message;
        return this;
    }
}
