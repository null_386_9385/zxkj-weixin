package com.zxkj.ssm.weixin.shop.service.impl;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.Resource;

import com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;
import com.zxkj.ssm.weixin.shop.cache.impl.SystemManager;
import com.zxkj.ssm.weixin.shop.dto.AreaDTO;
import com.zxkj.ssm.weixin.shop.mapper.dao.AreaMapperDao;
import com.zxkj.ssm.weixin.shop.service.AreaService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import static com.zxkj.ssm.weixin.shop.basic.Base.empty;
import static com.zxkj.ssm.weixin.shop.basic.Base.notEmpty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

@Service
@Transactional
public class AreaServiceImpl extends BasicServerImpl<AreaDTO,AreaMapperDao> implements AreaService{
	private AreaMapperDao areaMapperDao;
	@Resource private SystemManager systemManager;

	@Resource
	@Override
	public void setDao(AreaMapperDao dao) {
	this.areaMapperDao=dao;
	}

	@Override
	@Transactional(readOnly=true)
	public List<AreaDTO> findAllAreaListService() throws Exception {
		return areaMapperDao.findAllAreaList();
	}
	@Override
	@Transactional(readOnly=true)
	public List<AreaDTO> pageQueryAreaListService(Map<String,Object>map) throws Exception {
		return areaMapperDao.pageQueryAreaList(map);
	}
	@Override
	@Transactional(readOnly=true)
	public AreaDTO selectOneResultService() throws Exception {
		return areaMapperDao.selectOneResult();
	}
	@Override
	@Transactional(readOnly=true)
	public int findCountAreaService() throws Exception {
		return areaMapperDao.findCountArea();
	}
	/**加载区域数据*/
	public Map<String,AreaDTO> loadArea()throws Exception{
		   List<AreaDTO> rootAreas = Lists.newArrayList();
		   List<AreaDTO> areas =areaMapperDao.findAllAreaList();
	        for(AreaDTO a : areas){
	            if("0".equals(a.getPcode())) rootAreas.add(a);
	        }
	        if(rootAreas.size() == 0)  return null;
	        for(AreaDTO a : rootAreas){
	            getAreaByDigui2(a, areas);
	        }
	        Map<String, AreaDTO> map = new TreeMap<String, AreaDTO>();
	        for(AreaDTO a : rootAreas){
	            map.put(a.getCode(), a);
	        }
	        return map;
	}
	/**
	 * 递归加载省份下的：城市、区域、以后还会有街道的数据 areas 所有的地区列表
	 */
	private void getAreaByDigui2(AreaDTO item, final List<AreaDTO> areas){
		List<AreaDTO> children = Lists.newArrayList();
        for(AreaDTO a : areas) {
            if(item.getCode().equals(a.getPcode())){
                children.add(a);
            }
        }
		item.setChildren(children);
        if(children.size() == 0){
            return ;
        }
		for(AreaDTO a : children){
			getAreaByDigui2(a, areas);
		}
	}
	
	
	/**
	 * 根据省份编和城市编码获取县数据列表
	 */
	@Override
	public String ajaxGetCountyJsonDataService(String provinceCode,String cityCode)
			throws Exception {
			if(notEmpty(provinceCode) &&notEmpty(cityCode)){
			Map<String, AreaDTO> areaMap = getAreaDatasToCache();
			if(areaMap!=null && areaMap.size()>0){
				AreaDTO city = areaMap.get(provinceCode);
				if(city!=null && city.getChildren()!=null && city.getChildren().size()>0){
					for(int i=0;i<city.getChildren().size();i++){
						AreaDTO item = city.getChildren().get(i);
						if(item.getCode().equals(cityCode)){
							if(item.getChildren()!=null && item.getChildren().size()>0){
								String jsonStr =new ObjectMapper().writeValueAsString(item.getChildren());
								return (jsonStr);
							}
						}
					}
				}
			}
		}
		return ("{}");
	}
	
	/**
	 * 根据省份编码获取城市列表
	 */
	@Override
	public String ajaxGetCityJsonDataService(String provinceCode) throws Exception {
		if(empty(provinceCode)){
			throw new NullPointerException("provinceCode is null");
		}
        Map<String, AreaDTO> areaMap =getAreaDatasToCache();
		if(areaMap!=null && areaMap.size()>0){
			AreaDTO areaInfo = areaMap.get(provinceCode);
			if(areaInfo!=null && areaInfo.getChildren()!=null && areaInfo.getChildren().size()>0){
				String jsonStr = new ObjectMapper().writeValueAsString(areaInfo.getChildren());
				return (jsonStr);
			}
		}
		return ("{}");
	}





	public void loadAreaDatasToCache() throws Exception {
		systemManager.putCacheObject("areaChahe", (Serializable)
				loadArea());
	}



	public  Map<String, AreaDTO> getAreaDatasToCache() throws Exception {
		return systemManager.getCacheObject("areaChahe");
	}
}
