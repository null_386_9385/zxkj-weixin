package com.zxkj.ssm.weixin.shop;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@PropertySources(value={@PropertySource("classpath:config/config.properties"),
					    //@PropertySource("classpath:config/application-config.properties")//可以配置多个
		 })
@MapperScan(value={"com.zxkj.ssm.weixin.shop.mapper.dao"})
public class ApplicationConfiguration {
public static void main(String[] args) {
	new SpringApplication(ApplicationConfiguration.class).run(args);
}


	/**
	 * 配置 FastJson 转换器
	 * @return
     */
	/*@Bean
	public HttpMessageConverters fastjsonMessageConverter(){
		FastJsonHttpMessageConverter  fastJsonHttpMessageConverter=new FastJsonHttpMessageConverter();
			FastJsonConfig fastJsonConfig=new FastJsonConfig();
		fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
		fastJsonHttpMessageConverter.setFastJsonConfig(fastJsonConfig);
		HttpMessageConverter converter=fastJsonHttpMessageConverter;
		return new HttpMessageConverters(converter);
	}*/
}
