package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxOrder;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxOrderMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxOrderService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxOrderService实现类
*@Version
*/
@Service
public class  WxOrderServiceImpl extends BasicServerImpl<WxOrder,  WxOrderMapperDao> implements  WxOrderService {

@Resource
@Override
public void setDao( WxOrderMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
