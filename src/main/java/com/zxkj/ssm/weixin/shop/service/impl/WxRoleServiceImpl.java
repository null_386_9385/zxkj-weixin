package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxRole;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxRoleMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxRoleService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxRoleService实现类
*@Version
*/
@Service
public class  WxRoleServiceImpl extends BasicServerImpl<WxRole,  WxRoleMapperDao> implements  WxRoleService {

@Resource
@Override
public void setDao( WxRoleMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
