package com.zxkj.ssm.weixin.shop.dto;

import java.io.Serializable;
import java.util.List;
/**
 * 三级联动 省、市、区DTO
 */
public class AreaDTO extends com.zxkj.ssm.weixin.shop.entity.Area implements Serializable{
	private static final long serialVersionUID = 1L;
	private List<AreaDTO> children;// 子集合
	public void clear() {
		super.clear();
		if (children != null) {
			for (int i = 0; i < children.size(); i++) {
				children.get(i).clear();
			}
			children.clear();
			children = null;
		}
	}

	public List<AreaDTO> getChildren() {
		return children;
	}

	public void setChildren(List<AreaDTO> children) {
		this.children = children;
	}
}
