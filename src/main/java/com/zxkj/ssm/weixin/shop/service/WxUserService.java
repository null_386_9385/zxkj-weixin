package com.zxkj.ssm.weixin.shop.service;
import  com.zxkj.ssm.weixin.shop.basic.BasicService;
import  com.zxkj.ssm.weixin.shop.entity.WxUser;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxUserService
*@Version
*/
public interface WxUserService extends BasicService<WxUser> {

    }
