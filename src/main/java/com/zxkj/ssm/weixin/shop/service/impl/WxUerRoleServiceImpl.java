package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxUerRole;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxUerRoleMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxUerRoleService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxUerRoleService实现类
*@Version
*/
@Service
public class  WxUerRoleServiceImpl extends BasicServerImpl<WxUerRole,  WxUerRoleMapperDao> implements  WxUerRoleService {

@Resource
@Override
public void setDao( WxUerRoleMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
