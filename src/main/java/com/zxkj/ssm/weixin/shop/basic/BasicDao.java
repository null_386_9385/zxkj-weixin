package com.zxkj.ssm.weixin.shop.basic;
import java.util.List;
/**
 * 
 * @author 程序媛
 * @2018年6月24日
 * @param <E>
 * @Description:该接口提供业务逻辑最基本的服务，所有的业逻辑类都必须实现此接口，这样该业务逻辑类对应
 * 的controller就免去了写基本selectList、insert、update、toEdit、deletes麻烦s
 */
public interface BasicDao<E>{
	/**
	 * 添加
	 * 
	 * @param e
	 * @return
	 */
	public int insert(E e);

	
	/**
	 * 可选项插入
	 * @param e
	 * @return
	 */
	public int insertSelective(E e);
	/**
	 * 删除
	 * 
	 * @param e
	 * @return
	 */
	public int delete(E e);

	/**
	 * 批量删除
	 * @param list
	 * @return
     */
	public int deleteBatch(List<E> list);

	/**
	 * 修改
	 * 
	 * @param e
	 * @return
	 */
	public int update(E e);
	
	/**
	 * 修改 可选项修改
	 * 
	 * @param e
	 * @return
	 */
	public int updateByPrimaryKeySelective(E e);

	/**
	 * 查询一条记录
	 * 
	 * @param e
	 * @return
	 */
	public E selectOne(E e);

	/**
	 * 分页查询
	 * 
	 * @param e
	 * @return
	 */
	public List<E> selectPageList(E e);
	
	/**
	 * 根据条件查询所有
	 * @return
	 */
	public List<E> selectList(E e);

	public int selectPageCount (E e);
	/**
	 * 根据ID来删除一条记录
	 * @param id
	 */
	public int deleteById(Integer id);

	/**
	 * 根据ID查询一条记录
	 * @param id
	 * @return
	 */
	public E selectById(Integer id);
}
