package com.zxkj.ssm.weixin.shop.controller;
import com.zxkj.ssm.weixin.shop.dto.AreaDTO;
import com.zxkj.ssm.weixin.shop.service.AreaService;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


@Controller
@Log4j
public class AreaController {
    @Resource
    private AreaService areaService;

    @RequestMapping("/loadAreaCache")
    public void loadAreaCache(HttpServletRequest request, HttpServletResponse response,
                                Model model, ModelAndView view, Map<String, Object> map, Integer configId)throws Exception {

                     areaService.loadAreaDatasToCache();
                    log.info("加载aera数据到缓存");
    }


    @RequestMapping("/loadArea")
    @ResponseBody
    public  Map<String,AreaDTO> loadArea(HttpServletRequest request, HttpServletResponse response,
                              Model model, ModelAndView view, Map<String, Object> map, Integer configId)throws Exception {
            return areaService.getAreaDatasToCache();

    }
}