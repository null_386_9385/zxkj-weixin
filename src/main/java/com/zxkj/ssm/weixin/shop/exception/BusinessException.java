package com.zxkj.ssm.weixin.shop.exception;

/**
 * @author 程序员媛
 * @2017年9月7日
 * @Description: 400  Bad Request 
 */
public class BusinessException   extends RuntimeException{
	private static final long serialVersionUID = 1L;
	
	public BusinessException(String exmsg) {
		super(exmsg);
	}

}
