package com.zxkj.ssm.weixin.shop.entity;

import com.zxkj.ssm.weixin.shop.model.PagerModel;
import java.io.Serializable;
import java.util.Date;

public class WxOrder extends PagerModel implements Serializable {
    /**
     * 主键
     *
     * @mbg.generated
     */
    private Integer orderId;

    /**
     * 下单时间
     *
     * @mbg.generated
     */
    private Date orderCreateTime;

    /**
     * 1:未付款, 2:已付款但未发货, 3:已发货未确认收货, 4:确认收货了交易成功, 5:已取消(只有未付款才能取消)
     *
     * @mbg.generated
     */
    private Integer orderStatus;

    /**
     * 微信用户
     *
     * @mbg.generated
     */
    private Integer wuId;

    /**
     * 商品总金额
     *
     * @mbg.generated
     */
    private Long totalPrice;

    /**
     * 商品数量
     *
     * @mbg.generated
     */
    private Integer productTotal;

    private static final long serialVersionUID = 1L;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Date getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(Date orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getWuId() {
        return wuId;
    }

    public void setWuId(Integer wuId) {
        this.wuId = wuId;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getProductTotal() {
        return productTotal;
    }

    public void setProductTotal(Integer productTotal) {
        this.productTotal = productTotal;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", orderId=").append(orderId);
        sb.append(", orderCreateTime=").append(orderCreateTime);
        sb.append(", orderStatus=").append(orderStatus);
        sb.append(", wuId=").append(wuId);
        sb.append(", totalPrice=").append(totalPrice);
        sb.append(", productTotal=").append(productTotal);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        WxOrder other = (WxOrder) that;
        return (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getOrderCreateTime() == null ? other.getOrderCreateTime() == null : this.getOrderCreateTime().equals(other.getOrderCreateTime()))
            && (this.getOrderStatus() == null ? other.getOrderStatus() == null : this.getOrderStatus().equals(other.getOrderStatus()))
            && (this.getWuId() == null ? other.getWuId() == null : this.getWuId().equals(other.getWuId()))
            && (this.getTotalPrice() == null ? other.getTotalPrice() == null : this.getTotalPrice().equals(other.getTotalPrice()))
            && (this.getProductTotal() == null ? other.getProductTotal() == null : this.getProductTotal().equals(other.getProductTotal()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getOrderCreateTime() == null) ? 0 : getOrderCreateTime().hashCode());
        result = prime * result + ((getOrderStatus() == null) ? 0 : getOrderStatus().hashCode());
        result = prime * result + ((getWuId() == null) ? 0 : getWuId().hashCode());
        result = prime * result + ((getTotalPrice() == null) ? 0 : getTotalPrice().hashCode());
        result = prime * result + ((getProductTotal() == null) ? 0 : getProductTotal().hashCode());
        return result;
    }
}