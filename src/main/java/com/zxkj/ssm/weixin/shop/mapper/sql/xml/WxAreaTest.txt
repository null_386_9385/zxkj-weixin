目标package: com.zxkj.ssm.weixin.shop.mapper.sql.xml

当前时间：
2018-7-10
17:23:22
2018-07-10 17:23:22

所有配置的属性信息:
targetPackage - com.zxkj.ssm.weixin.shop.mapper.sql.xml
templateFormatter - com.zxkj.ssm.weixin.shop.mybatis.generator.formatter.FreemarkerTemplateFormatter
templatePath - template/test/test-one.ftl
targetProject - D:/EclipseWorkspace2/zxkj-weixin/src/main/java
fileName - ${tableClass.shortClassName}Test.txt

实体和表的信息：
表名：wx_area
变量名：wxArea
小写名：wxarea
类名：WxArea
全名：com.zxkj.ssm.weixin.shop.entity.WxArea
包名：com.zxkj.ssm.weixin.shop.entity

列的信息：
=====================================
主键：
    -------------------------------------
    列名：id
    列类型：INTEGER
    字段名：id
    注释：主键
    类型包名：java.lang
    类型短名：Integer
    类型全名：java.lang.Integer
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：false
    是否为字符串列：false
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：10
    列精度：0

基础列：
    -------------------------------------
    列名：code
    列类型：VARCHAR
    字段名：code
    注释：
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：45
    列精度：0
    -------------------------------------
    列名：pcode
    列类型：VARCHAR
    字段名：pcode
    注释：
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：45
    列精度：0
    -------------------------------------
    列名：name
    列类型：VARCHAR
    字段名：name
    注释：
    类型包名：java.lang
    类型短名：String
    类型全名：java.lang.String
    是否主键：false
    是否可空：false
    是否为BLOB列：false
    是否为String列：true
    是否为字符串列：true
    是否为日期列：false
    是否为时间列：false
    是否为序列列：false
    列长度：45
    列精度：0

Blob列：

=====================================
全部列：
列名 - 字段名
    id - id
    code - code
    pcode - pcode
    name - name
