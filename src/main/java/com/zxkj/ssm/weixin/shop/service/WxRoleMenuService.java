package com.zxkj.ssm.weixin.shop.service;
import  com.zxkj.ssm.weixin.shop.basic.BasicService;
import  com.zxkj.ssm.weixin.shop.entity.WxRoleMenu;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxRoleMenuService
*@Version
*/
public interface WxRoleMenuService extends BasicService<WxRoleMenu> {

    }
