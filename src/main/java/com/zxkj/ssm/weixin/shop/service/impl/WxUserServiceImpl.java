package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxUser;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxUserMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxUserService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxUserService实现类
*@Version
*/
@Service
public class  WxUserServiceImpl extends BasicServerImpl<WxUser,  WxUserMapperDao> implements  WxUserService {

@Resource
@Override
public void setDao( WxUserMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
