package com.zxkj.ssm.weixin.shop.service;

import com.zxkj.ssm.weixin.shop.basic.BasicService;
import com.zxkj.ssm.weixin.shop.dto.AreaDTO;

import java.util.List;
import java.util.Map;

/**
 *  区域Service接口
 */
public interface AreaService extends BasicService<AreaDTO>{
	public List<AreaDTO> findAllAreaListService()throws Exception;
	
	public List<AreaDTO>  pageQueryAreaListService(Map<String, Object> map)throws Exception;
	
	public AreaDTO selectOneResultService()throws Exception;
	
	public int findCountAreaService()throws Exception;
	
	public Map<String,AreaDTO> loadArea()throws Exception;

	/**
	 * 根据省份编和城市编码获取县数据列表
	 */
	public String ajaxGetCountyJsonDataService(String provinceCode, String cityCode)throws Exception;

	/**
	 * 根据省份编码获取城市列表
	 */
	public String ajaxGetCityJsonDataService(String provinceCode)throws Exception;


	/**
	 * 加载所有的区域数据到 缓存中
	 */
	public  Map<String, AreaDTO> getAreaDatasToCache() throws Exception;
	/**
	 * 从缓存中读取数据
	 */
	public void loadAreaDatasToCache() throws Exception;
}
