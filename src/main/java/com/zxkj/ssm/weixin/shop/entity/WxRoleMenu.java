package com.zxkj.ssm.weixin.shop.entity;

import com.zxkj.ssm.weixin.shop.model.PagerModel;
import java.io.Serializable;

public class WxRoleMenu extends PagerModel implements Serializable {
    /**
     * 主键
     *
     * @mbg.generated
     */
    private Integer roleMenuId;

    /**
     * 角色ID
     *
     * @mbg.generated
     */
    private Integer roleId;

    /**
     * 菜单ID
     *
     * @mbg.generated
     */
    private Integer menuId;

    private static final long serialVersionUID = 1L;

    public Integer getRoleMenuId() {
        return roleMenuId;
    }

    public void setRoleMenuId(Integer roleMenuId) {
        this.roleMenuId = roleMenuId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", roleMenuId=").append(roleMenuId);
        sb.append(", roleId=").append(roleId);
        sb.append(", menuId=").append(menuId);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        WxRoleMenu other = (WxRoleMenu) that;
        return (this.getRoleMenuId() == null ? other.getRoleMenuId() == null : this.getRoleMenuId().equals(other.getRoleMenuId()))
            && (this.getRoleId() == null ? other.getRoleId() == null : this.getRoleId().equals(other.getRoleId()))
            && (this.getMenuId() == null ? other.getMenuId() == null : this.getMenuId().equals(other.getMenuId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getRoleMenuId() == null) ? 0 : getRoleMenuId().hashCode());
        result = prime * result + ((getRoleId() == null) ? 0 : getRoleId().hashCode());
        result = prime * result + ((getMenuId() == null) ? 0 : getMenuId().hashCode());
        return result;
    }
}