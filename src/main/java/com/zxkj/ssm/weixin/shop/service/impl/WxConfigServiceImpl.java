package  com.zxkj.ssm.weixin.shop.service.impl;
import javax.annotation.Resource;
import  com.zxkj.ssm.weixin.shop.entity.WxConfig;
import  com.zxkj.ssm.weixin.shop.mapper.dao.WxConfigMapperDao;
import  com.zxkj.ssm.weixin.shop.service.WxConfigService;
import org.springframework.stereotype.Service;
import  com.zxkj.ssm.weixin.shop.basic.BasicServerImpl;

/**
*@Author 程序媛
*@Date 2018/7/10
*@Description  WxConfigService实现类
*@Version
*/
@Service
public class  WxConfigServiceImpl extends BasicServerImpl<WxConfig,  WxConfigMapperDao> implements  WxConfigService {

@Resource
@Override
public void setDao( WxConfigMapperDao  mapperDao) {
this.dao= mapperDao;
}


}
