package com.zxkj.ssm.weixin.shop.entity;

import com.zxkj.ssm.weixin.shop.model.PagerModel;
import java.io.Serializable;
import java.util.Date;

public class WxCart extends PagerModel implements Serializable {
    /**
     * 主键ID
     *
     * @mbg.generated
     */
    private Integer cartId;

    /**
     * 微信用户ID
     *
     * @mbg.generated
     */
    private Integer wuId;

    /**
     * 创建时间
     *
     * @mbg.generated
     */
    private Date cartCreateTime;

    private static final long serialVersionUID = 1L;

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public Integer getWuId() {
        return wuId;
    }

    public void setWuId(Integer wuId) {
        this.wuId = wuId;
    }

    public Date getCartCreateTime() {
        return cartCreateTime;
    }

    public void setCartCreateTime(Date cartCreateTime) {
        this.cartCreateTime = cartCreateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", cartId=").append(cartId);
        sb.append(", wuId=").append(wuId);
        sb.append(", cartCreateTime=").append(cartCreateTime);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        WxCart other = (WxCart) that;
        return (this.getCartId() == null ? other.getCartId() == null : this.getCartId().equals(other.getCartId()))
            && (this.getWuId() == null ? other.getWuId() == null : this.getWuId().equals(other.getWuId()))
            && (this.getCartCreateTime() == null ? other.getCartCreateTime() == null : this.getCartCreateTime().equals(other.getCartCreateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getCartId() == null) ? 0 : getCartId().hashCode());
        result = prime * result + ((getWuId() == null) ? 0 : getWuId().hashCode());
        result = prime * result + ((getCartCreateTime() == null) ? 0 : getCartCreateTime().hashCode());
        return result;
    }
}