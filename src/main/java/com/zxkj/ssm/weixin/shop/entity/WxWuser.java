package com.zxkj.ssm.weixin.shop.entity;

import com.zxkj.ssm.weixin.shop.model.PagerModel;
import java.io.Serializable;
import java.util.Date;

public class WxWuser extends PagerModel implements Serializable {
    /**
     * 主键
     *
     * @mbg.generated
     */
    private Integer wuId;

    /**
     * 昵称
     *
     * @mbg.generated
     */
    private String wuNickname;

    /**
     * 微信号
     *
     * @mbg.generated
     */
    private String wuOpenid;

    /**
     * 性别
     *
     * @mbg.generated
     */
    private String wuSex;

    /**
     * 城市
     *
     * @mbg.generated
     */
    private String wuCity;

    /**
     * 国家
     *
     * @mbg.generated
     */
    private String wuCountry;

    /**
     * 省份
     *
     * @mbg.generated
     */
    private String wuProvince;

    /**
     * 语言
     *
     * @mbg.generated
     */
    private String wuLanguage;

    /**
     * 头像
     *
     * @mbg.generated
     */
    private String wuHeadimgUrl;

    /**
     * 关注时间
     *
     * @mbg.generated
     */
    private Date wuSubscribeTime;

    /**
     * 取消关注时间
     *
     * @mbg.generated
     */
    private Date wuUnsubscribeTime;

    /**
     * 微信分组id
     *
     * @mbg.generated
     */
    private Integer wuGroupid;

    /**
     * 状态
     *
     * @mbg.generated
     */
    private Integer wuStatus;

    private static final long serialVersionUID = 1L;

    public Integer getWuId() {
        return wuId;
    }

    public void setWuId(Integer wuId) {
        this.wuId = wuId;
    }

    public String getWuNickname() {
        return wuNickname;
    }

    public void setWuNickname(String wuNickname) {
        this.wuNickname = wuNickname;
    }

    public String getWuOpenid() {
        return wuOpenid;
    }

    public void setWuOpenid(String wuOpenid) {
        this.wuOpenid = wuOpenid;
    }

    public String getWuSex() {
        return wuSex;
    }

    public void setWuSex(String wuSex) {
        this.wuSex = wuSex;
    }

    public String getWuCity() {
        return wuCity;
    }

    public void setWuCity(String wuCity) {
        this.wuCity = wuCity;
    }

    public String getWuCountry() {
        return wuCountry;
    }

    public void setWuCountry(String wuCountry) {
        this.wuCountry = wuCountry;
    }

    public String getWuProvince() {
        return wuProvince;
    }

    public void setWuProvince(String wuProvince) {
        this.wuProvince = wuProvince;
    }

    public String getWuLanguage() {
        return wuLanguage;
    }

    public void setWuLanguage(String wuLanguage) {
        this.wuLanguage = wuLanguage;
    }

    public String getWuHeadimgUrl() {
        return wuHeadimgUrl;
    }

    public void setWuHeadimgUrl(String wuHeadimgUrl) {
        this.wuHeadimgUrl = wuHeadimgUrl;
    }

    public Date getWuSubscribeTime() {
        return wuSubscribeTime;
    }

    public void setWuSubscribeTime(Date wuSubscribeTime) {
        this.wuSubscribeTime = wuSubscribeTime;
    }

    public Date getWuUnsubscribeTime() {
        return wuUnsubscribeTime;
    }

    public void setWuUnsubscribeTime(Date wuUnsubscribeTime) {
        this.wuUnsubscribeTime = wuUnsubscribeTime;
    }

    public Integer getWuGroupid() {
        return wuGroupid;
    }

    public void setWuGroupid(Integer wuGroupid) {
        this.wuGroupid = wuGroupid;
    }

    public Integer getWuStatus() {
        return wuStatus;
    }

    public void setWuStatus(Integer wuStatus) {
        this.wuStatus = wuStatus;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", wuId=").append(wuId);
        sb.append(", wuNickname=").append(wuNickname);
        sb.append(", wuOpenid=").append(wuOpenid);
        sb.append(", wuSex=").append(wuSex);
        sb.append(", wuCity=").append(wuCity);
        sb.append(", wuCountry=").append(wuCountry);
        sb.append(", wuProvince=").append(wuProvince);
        sb.append(", wuLanguage=").append(wuLanguage);
        sb.append(", wuHeadimgUrl=").append(wuHeadimgUrl);
        sb.append(", wuSubscribeTime=").append(wuSubscribeTime);
        sb.append(", wuUnsubscribeTime=").append(wuUnsubscribeTime);
        sb.append(", wuGroupid=").append(wuGroupid);
        sb.append(", wuStatus=").append(wuStatus);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        WxWuser other = (WxWuser) that;
        return (this.getWuId() == null ? other.getWuId() == null : this.getWuId().equals(other.getWuId()))
            && (this.getWuNickname() == null ? other.getWuNickname() == null : this.getWuNickname().equals(other.getWuNickname()))
            && (this.getWuOpenid() == null ? other.getWuOpenid() == null : this.getWuOpenid().equals(other.getWuOpenid()))
            && (this.getWuSex() == null ? other.getWuSex() == null : this.getWuSex().equals(other.getWuSex()))
            && (this.getWuCity() == null ? other.getWuCity() == null : this.getWuCity().equals(other.getWuCity()))
            && (this.getWuCountry() == null ? other.getWuCountry() == null : this.getWuCountry().equals(other.getWuCountry()))
            && (this.getWuProvince() == null ? other.getWuProvince() == null : this.getWuProvince().equals(other.getWuProvince()))
            && (this.getWuLanguage() == null ? other.getWuLanguage() == null : this.getWuLanguage().equals(other.getWuLanguage()))
            && (this.getWuHeadimgUrl() == null ? other.getWuHeadimgUrl() == null : this.getWuHeadimgUrl().equals(other.getWuHeadimgUrl()))
            && (this.getWuSubscribeTime() == null ? other.getWuSubscribeTime() == null : this.getWuSubscribeTime().equals(other.getWuSubscribeTime()))
            && (this.getWuUnsubscribeTime() == null ? other.getWuUnsubscribeTime() == null : this.getWuUnsubscribeTime().equals(other.getWuUnsubscribeTime()))
            && (this.getWuGroupid() == null ? other.getWuGroupid() == null : this.getWuGroupid().equals(other.getWuGroupid()))
            && (this.getWuStatus() == null ? other.getWuStatus() == null : this.getWuStatus().equals(other.getWuStatus()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getWuId() == null) ? 0 : getWuId().hashCode());
        result = prime * result + ((getWuNickname() == null) ? 0 : getWuNickname().hashCode());
        result = prime * result + ((getWuOpenid() == null) ? 0 : getWuOpenid().hashCode());
        result = prime * result + ((getWuSex() == null) ? 0 : getWuSex().hashCode());
        result = prime * result + ((getWuCity() == null) ? 0 : getWuCity().hashCode());
        result = prime * result + ((getWuCountry() == null) ? 0 : getWuCountry().hashCode());
        result = prime * result + ((getWuProvince() == null) ? 0 : getWuProvince().hashCode());
        result = prime * result + ((getWuLanguage() == null) ? 0 : getWuLanguage().hashCode());
        result = prime * result + ((getWuHeadimgUrl() == null) ? 0 : getWuHeadimgUrl().hashCode());
        result = prime * result + ((getWuSubscribeTime() == null) ? 0 : getWuSubscribeTime().hashCode());
        result = prime * result + ((getWuUnsubscribeTime() == null) ? 0 : getWuUnsubscribeTime().hashCode());
        result = prime * result + ((getWuGroupid() == null) ? 0 : getWuGroupid().hashCode());
        result = prime * result + ((getWuStatus() == null) ? 0 : getWuStatus().hashCode());
        return result;
    }
}