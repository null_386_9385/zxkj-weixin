package com.zxkj.ssm.weixin.shop.entity;

import com.zxkj.ssm.weixin.shop.model.PagerModel;
import java.io.Serializable;
import java.util.Date;

public class WxProduct extends PagerModel implements Serializable {
    /**
     * 主键
     *
     * @mbg.generated
     */
    private Integer productId;

    /**
     * 商品名称
     *
     * @mbg.generated
     */
    private String productName;

    /**
     * 商品摘要
     *
     * @mbg.generated
     */
    private String productDigest;

    /**
     * 原价
     *
     * @mbg.generated
     */
    private Long productPrice;

    /**
     * 促销价
     *
     * @mbg.generated
     */
    private Long prodectCurrentPrice;

    /**
     * 商品封面
     *
     * @mbg.generated
     */
    private String prodectCover;

    /**
     * 创建时间
     *
     * @mbg.generated
     */
    private Date prodectCreateTime;

    /**
     * 修改时间
     *
     * @mbg.generated
     */
    private Date productModifyTime;

    /**
     * 是否上架
     *
     * @mbg.generated
     */
    private Boolean productShelves;

    /**
     * 库存数量
     *
     * @mbg.generated
     */
    private Integer productStock;

    /**
     * 库存状态
     *
     * @mbg.generated
     */
    private Boolean productStockStuats;

    /**
     * 商品内容
     *
     * @mbg.generated
     */
    private String prodectContent;

    private static final long serialVersionUID = 1L;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDigest() {
        return productDigest;
    }

    public void setProductDigest(String productDigest) {
        this.productDigest = productDigest;
    }

    public Long getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Long productPrice) {
        this.productPrice = productPrice;
    }

    public Long getProdectCurrentPrice() {
        return prodectCurrentPrice;
    }

    public void setProdectCurrentPrice(Long prodectCurrentPrice) {
        this.prodectCurrentPrice = prodectCurrentPrice;
    }

    public String getProdectCover() {
        return prodectCover;
    }

    public void setProdectCover(String prodectCover) {
        this.prodectCover = prodectCover;
    }

    public Date getProdectCreateTime() {
        return prodectCreateTime;
    }

    public void setProdectCreateTime(Date prodectCreateTime) {
        this.prodectCreateTime = prodectCreateTime;
    }

    public Date getProductModifyTime() {
        return productModifyTime;
    }

    public void setProductModifyTime(Date productModifyTime) {
        this.productModifyTime = productModifyTime;
    }

    public Boolean getProductShelves() {
        return productShelves;
    }

    public void setProductShelves(Boolean productShelves) {
        this.productShelves = productShelves;
    }

    public Integer getProductStock() {
        return productStock;
    }

    public void setProductStock(Integer productStock) {
        this.productStock = productStock;
    }

    public Boolean getProductStockStuats() {
        return productStockStuats;
    }

    public void setProductStockStuats(Boolean productStockStuats) {
        this.productStockStuats = productStockStuats;
    }

    public String getProdectContent() {
        return prodectContent;
    }

    public void setProdectContent(String prodectContent) {
        this.prodectContent = prodectContent;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", productId=").append(productId);
        sb.append(", productName=").append(productName);
        sb.append(", productDigest=").append(productDigest);
        sb.append(", productPrice=").append(productPrice);
        sb.append(", prodectCurrentPrice=").append(prodectCurrentPrice);
        sb.append(", prodectCover=").append(prodectCover);
        sb.append(", prodectCreateTime=").append(prodectCreateTime);
        sb.append(", productModifyTime=").append(productModifyTime);
        sb.append(", productShelves=").append(productShelves);
        sb.append(", productStock=").append(productStock);
        sb.append(", productStockStuats=").append(productStockStuats);
        sb.append(", prodectContent=").append(prodectContent);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        WxProduct other = (WxProduct) that;
        return (this.getProductId() == null ? other.getProductId() == null : this.getProductId().equals(other.getProductId()))
            && (this.getProductName() == null ? other.getProductName() == null : this.getProductName().equals(other.getProductName()))
            && (this.getProductDigest() == null ? other.getProductDigest() == null : this.getProductDigest().equals(other.getProductDigest()))
            && (this.getProductPrice() == null ? other.getProductPrice() == null : this.getProductPrice().equals(other.getProductPrice()))
            && (this.getProdectCurrentPrice() == null ? other.getProdectCurrentPrice() == null : this.getProdectCurrentPrice().equals(other.getProdectCurrentPrice()))
            && (this.getProdectCover() == null ? other.getProdectCover() == null : this.getProdectCover().equals(other.getProdectCover()))
            && (this.getProdectCreateTime() == null ? other.getProdectCreateTime() == null : this.getProdectCreateTime().equals(other.getProdectCreateTime()))
            && (this.getProductModifyTime() == null ? other.getProductModifyTime() == null : this.getProductModifyTime().equals(other.getProductModifyTime()))
            && (this.getProductShelves() == null ? other.getProductShelves() == null : this.getProductShelves().equals(other.getProductShelves()))
            && (this.getProductStock() == null ? other.getProductStock() == null : this.getProductStock().equals(other.getProductStock()))
            && (this.getProductStockStuats() == null ? other.getProductStockStuats() == null : this.getProductStockStuats().equals(other.getProductStockStuats()))
            && (this.getProdectContent() == null ? other.getProdectContent() == null : this.getProdectContent().equals(other.getProdectContent()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProductId() == null) ? 0 : getProductId().hashCode());
        result = prime * result + ((getProductName() == null) ? 0 : getProductName().hashCode());
        result = prime * result + ((getProductDigest() == null) ? 0 : getProductDigest().hashCode());
        result = prime * result + ((getProductPrice() == null) ? 0 : getProductPrice().hashCode());
        result = prime * result + ((getProdectCurrentPrice() == null) ? 0 : getProdectCurrentPrice().hashCode());
        result = prime * result + ((getProdectCover() == null) ? 0 : getProdectCover().hashCode());
        result = prime * result + ((getProdectCreateTime() == null) ? 0 : getProdectCreateTime().hashCode());
        result = prime * result + ((getProductModifyTime() == null) ? 0 : getProductModifyTime().hashCode());
        result = prime * result + ((getProductShelves() == null) ? 0 : getProductShelves().hashCode());
        result = prime * result + ((getProductStock() == null) ? 0 : getProductStock().hashCode());
        result = prime * result + ((getProductStockStuats() == null) ? 0 : getProductStockStuats().hashCode());
        result = prime * result + ((getProdectContent() == null) ? 0 : getProdectContent().hashCode());
        return result;
    }
}